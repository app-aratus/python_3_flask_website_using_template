# README #

This is a Python 3 Flask Web application that uses templates.

### What is this repository for? ###

* Quick summary

When started, this application produces a Flask website that utilizes a standardized template.

* Version

1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Install the Python 3 environment with Anaconda

pip install flask

* How to run it

./start_flask.sh

./test_flask.sh

or

point your browser to- 

http://127.0.0.1:5000/

* What file is being called when it is run?

app.py

### Snapshots ###

* Startup From MacOSX Terminal
![Flask application template startup from command line](images/flask_application_template_startup_from_command_line.png)

* Test From MacOSX Terminal
![Flask application template test from command line](images/flask_application_template_test_from_command_line.png)

* Index Page From A Browser
![Flask application template index page from browser](images/flask_application_template_index_page.png)

* About Page From A Browser
![Flask application template about page from browser](images/flask_application_template_about_page.png)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
